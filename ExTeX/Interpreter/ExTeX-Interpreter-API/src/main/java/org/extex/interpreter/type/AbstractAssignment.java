/*
 * Copyright (C) 2003-2009 The ExTeX Group and individual authors listed below
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package org.extex.interpreter.type;

import org.extex.core.exception.helping.HelpingException;
import org.extex.framework.configuration.exception.ConfigurationException;
import org.extex.interpreter.Flags;
import org.extex.interpreter.TokenSource;
import org.extex.interpreter.context.Context;
import org.extex.scanner.type.token.CodeToken;
import org.extex.scanner.type.token.Token;
import org.extex.typesetter.Typesetter;
import org.extex.typesetter.exception.TypesetterException;

/**
 * This is the base class for assignments. The assignments are implemented like
 * any Code with the exception that the method <tt>assign</tt> is used instead
 * of the method <tt>execute</tt>.
 *
 * <p>
 * This abstract class takes care of the treatment of the
 * <tt>\afterassignment</tt> token and the <tt>\globaldefs</tt> declaration.
 * </p>
 *
 *
 * <doc name="globaldefs" type="register"> <h3>The Count Parameter
 * <tt>\globaldefs</tt></h3>
 * <p>
 * The count register <tt>\globaldefs</tt> contains the indicator that an
 * assignment should be performed globally. If its value is greater than zero
 * then all assignments are global. Otherwise the grouping is honored. In this
 * sense setting <tt>\globaldefs</tt> to a positive value implicitly prefixes
 * all assignments with <tt>\global</tt>.
 * </p>
 *
 * <h4>Syntax</h4> The formal description of this primitive is the following:
 *
 * <pre class="syntax">
 *    &lang;globaldefs&rang;
 *       &rarr; <tt>\globaldefs</tt> {@linkplain
 *        org.extex.interpreter.TokenSource#getOptionalEquals(Context)
 *        &lang;equals&rang;} {@linkplain
 *        org.extex.base.parser.ConstantCountParser#parseNumber(Context,TokenSource,Typesetter)
 *        &lang;number&rang;}  </pre>
 *
 * <h4>Examples</h4>
 *
 * <pre class="TeXSample">
 *    \globaldefs=1  </pre>
 *
 * </doc>
 *
 *
 * @author <a href="mailto:gene@gerd-neugebauer.de">Gerd Neugebauer</a>
 * @version $Revision$
 */
public abstract class AbstractAssignment extends AbstractCode {

    /**
     * The field <tt>serialVersionUID</tt> contains the version number for
     * serialization.
     */
    private static final long serialVersionUID = 2009L;

    /**
     * Creates a new object.
     *
     * @param token the initial token for the primitive
     */
    public AbstractAssignment(CodeToken token) {

        super(token);
    }

    /**
     * The method <tt>assign</tt> is the core of the functionality of
     * {@link #execute(Flags, Context, TokenSource, Typesetter) execute()}. This
     * method is preferable to <tt>execute()</tt> since the <tt>execute()</tt>
     * method provided in this class takes care of <tt>\afterassignment</tt> and
     * <tt>\globaldefs</tt> as well.
     *
     * @param prefix the prefix controlling the execution
     * @param context the interpreter context
     * @param source the token source
     * @param typesetter the typesetter
     *
     * @throws ConfigurationException in case of an configuration error
     * @throws HelpingException in case of an error
     * @throws TypesetterException in case of an error in the typesetter
     */
    public abstract void assign(Flags prefix, Context context,
            TokenSource source, Typesetter typesetter)
            throws ConfigurationException,
                HelpingException,
                TypesetterException;

    /**
     * {@inheritDoc}
     *
     * @see org.extex.interpreter.type.Code#execute(org.extex.interpreter.Flags,
     *      org.extex.interpreter.context.Context,
     *      org.extex.interpreter.TokenSource, org.extex.typesetter.Typesetter)
     */
    @Override
    public final void execute(Flags prefix, Context context,
            TokenSource source, Typesetter typesetter)
            throws HelpingException,
                TypesetterException {

        long globaldef = context.getCount("globaldefs").getValue();
        if (globaldef != 0) {
            prefix.setGlobal((globaldef > 0));
        }

        assign(prefix, context, source, typesetter);

        Token afterassignment = context.getAfterassignment();
        if (afterassignment != null) {
            context.setAfterassignment(null);
            source.push(afterassignment);
        }
    }

}
